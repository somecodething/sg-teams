package org.MC3.sg;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Random;

import org.MC3.sg.scatter.Scatter;
import org.bukkit.Bukkit;
import org.bukkit.Location;

public class Utils {
	public static int randomInt(int min, int max) {
		return new Random().nextInt((max - min) + 1) + min;
	}
	
	public static void setLine(String update, String value) throws Exception {
		BufferedReader file = new BufferedReader(new FileReader("/root/minecraft/" + Bukkit.getServerName() + "/server.properties"));
		String line;
		String input = "";
		while((line = file.readLine()) != null) {
			input += line + System.lineSeparator();
		}
		String[] input_s = input.split(System.lineSeparator());
		for(String ln : input_s) {
			if(ln.contains(update+"=")) {
				input = input.replace(ln, update+"=" + value);
			}
		}
		FileOutputStream os = new FileOutputStream("/root/minecraft/" + Bukkit.getServerName() + "/server.properties");
		os.write(input.getBytes());
		file.close();
		os.close();
	}
	
	public static boolean locIsNearChests(Location loc) {
		for(Location ll : Scatter.ChestLocations) {
			if(loc.distance(ll) <= 20) {
				return true;
			}
		}
		
		return false;
	}
}