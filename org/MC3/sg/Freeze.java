package org.MC3.sg;

import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Freeze implements Listener {
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player player = e.getPlayer();
		if(frozen.contains(player)) {
			if(e.getTo().getX() != e.getFrom().getX() || e.getTo().getZ() != e.getFrom().getZ()) {
				player.teleport(e.getFrom());
			}
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		if(frozen.contains(player)) {
			e.setCancelled(true);
		}
	}
	
	public static ArrayList<Player> frozen = new ArrayList<Player>();
	
	public static void set(Player player, boolean value) {
		if(value == true) {
			frozen.add(player);
			player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 24000, 255, true, false));
			player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 24000, 0, true, false));
		} else {
			frozen.remove(player);
			player.removePotionEffect(PotionEffectType.SLOW);
			player.removePotionEffect(PotionEffectType.BLINDNESS);
		}
	}
}