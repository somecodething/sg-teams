package org.MC3.sg;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(label.equalsIgnoreCase("tp")) {
			if(sender instanceof Player) {
				Player player = (Player) sender;
				if(player.getGameMode() == GameMode.SPECTATOR) {
					if(args.length == 1) {
						Player teleportTo = Bukkit.getPlayer(args[0]);
						if(teleportTo != null && teleportTo.isOnline()) {
							player.teleport(teleportTo);
						} else {
							player.sendMessage("�cThat player is not online.");
						}
					} else {
						player.sendMessage("�cUsage: /tp [player]");
					}
				} else {
					player.sendMessage("�cYou are not a spectator.");
				}
			}
		}
		return true;
	}
}