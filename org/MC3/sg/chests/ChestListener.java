package org.MC3.sg.chests;

import java.util.ArrayList;
import java.util.HashMap;

import org.MC3.sg.Utils;
import org.MC3.sg.Vars;
import org.MC3.sg.game.Spec;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

import me.Swedz.api.API;

public class ChestListener implements Listener {
	HashMap<Player, Location> lastClickLoc = new HashMap<Player, Location>();
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		if(Vars.ingame == true && e.isCancelled() == false) {
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				lastClickLoc.put(player, e.getClickedBlock().getLocation());
			}
		}
	}
	
	@EventHandler
	public void onOpen(InventoryOpenEvent e) {
		Player player = (Player) e.getPlayer();
		if(Vars.ingame == true && e.isCancelled() == false && !Spec.specs.contains(player)) {
			if(e.getInventory().getType() == InventoryType.CHEST && !Vars.openedChests.contains(lastClickLoc.get(player))) {
				for(int slot = 0; slot < 27; slot++) {
					e.getInventory().setItem(slot, new ItemStack(Material.AIR));
				}
				
				int itemCounts = Utils.randomInt(4, 6);
				ArrayList<Integer> usedSlots = new ArrayList<Integer>();
				for(int x = 0; x < itemCounts; x++) {
					double d = Math.random();
					ItemStack randomItem;
					if(d < (1-Vars.tier2Chance)) {
						Tier1 valueType = Tier1.randomItem();
						randomItem = new ItemStack(Material.valueOf(valueType.toString()), Utils.randomInt(1, valueType.getValue()));
						if(randomItem.getType().equals(Material.ENCHANTED_BOOK)) {
							String enchantName = Enchantments.randomEnchant().toString();
							
							EnchantmentStorageMeta meta = (EnchantmentStorageMeta)randomItem.getItemMeta();
							if(enchantName == "ARROW_DAMAGE") {
								meta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 1, true);
							} else if(enchantName == "ARROW_FIRE") {
								meta.addStoredEnchant(Enchantment.ARROW_FIRE, 1, true);
							} else if(enchantName == "ARROW_INFINITE") {
								meta.addStoredEnchant(Enchantment.ARROW_INFINITE, 1, true);
							} else if(enchantName == "DAMAGE_ALL") {
								meta.addStoredEnchant(Enchantment.DAMAGE_ALL, 1, true);
							} else if(enchantName == "FIRE_ASPECT") {
								meta.addStoredEnchant(Enchantment.FIRE_ASPECT, 1, true);
							} else if(enchantName == "KNOCKBACK") {
								meta.addStoredEnchant(Enchantment.KNOCKBACK, 1, true);
							} else if(enchantName == "PROTECTION_ENVIRONMENTAL") {
								meta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
							} else if(enchantName == "THORNS") {
								meta.addStoredEnchant(Enchantment.THORNS, 1, true);
							}
							
							randomItem.setItemMeta(meta);
						} else {
							randomItem = API.getItemAPI().createItem(randomItem, null, new String[] {}, null, false, false);
						}
					} else {
						Tier2 valueType = Tier2.randomItem();
						randomItem = new ItemStack(Material.valueOf(valueType.toString()), Utils.randomInt(1, valueType.getValue()));
						if(randomItem.getType().equals(Material.ENCHANTED_BOOK)) {
							String enchantName = Enchantments.randomEnchant().toString();
							
							EnchantmentStorageMeta meta = (EnchantmentStorageMeta)randomItem.getItemMeta();
							if(enchantName == "ARROW_DAMAGE") {
								meta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 1, true);
							} else if(enchantName == "ARROW_FIRE") {
								meta.addStoredEnchant(Enchantment.ARROW_FIRE, 1, true);
							} else if(enchantName == "ARROW_INFINITE") {
								meta.addStoredEnchant(Enchantment.ARROW_INFINITE, 1, true);
							} else if(enchantName == "DAMAGE_ALL") {
								meta.addStoredEnchant(Enchantment.DAMAGE_ALL, 1, true);
							} else if(enchantName == "FIRE_ASPECT") {
								meta.addStoredEnchant(Enchantment.FIRE_ASPECT, 1, true);
							} else if(enchantName == "KNOCKBACK") {
								meta.addStoredEnchant(Enchantment.KNOCKBACK, 1, true);
							} else if(enchantName == "PROTECTION_ENVIRONMENTAL") {
								meta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
							} else if(enchantName == "THORNS") {
								meta.addStoredEnchant(Enchantment.THORNS, 1, true);
							}
							
							randomItem.setItemMeta(meta);
						} else {
							randomItem = API.getItemAPI().createItem(randomItem, null, new String[] {}, null, false, false);
						}
					}
					
					int randomSlot = -1;
					while(randomSlot == -1) {
						int slot = Utils.randomInt(0, 26);
						if(!usedSlots.contains(slot)) {
							randomSlot = slot;
							break;
						}
					}
					
					e.getInventory().setItem(randomSlot, randomItem);
				}
				
				Vars.openedChests.add(lastClickLoc.get(player));
			}
		}
	}
}