package org.MC3.sg.chests;

import org.MC3.sg.Utils;

public enum Tier1 {
	CHAINMAIL_HELMET(1),
	CHAINMAIL_CHESTPLATE(1),
	CHAINMAIL_LEGGINGS(1),
	CHAINMAIL_BOOTS(1),
	STRING(2),
	STICK(2),
	BOW(1),
	WATER_BUCKET(1),
	GOLDEN_CARROT(1),
	STONE_SWORD(1),
	MELON(10),
	FLINT(4),
	EXP_BOTTLE(8),
	ARROW(6),
	WOOD_SWORD(1),
	STONE_AXE(1),
	WOOD_AXE(1),
	ENCHANTED_BOOK(1),
	COOKED_BEEF(6),
	IRON_AXE(1),
	FEATHER(3),
	PUMPKIN_PIE(4),
	GOLD_HELMET(1),
	GOLD_CHESTPLATE(1),
	GOLD_LEGGINGS(1),
	GOLD_BOOTS(1),
	LEATHER_HELMET(1),
	LEATHER_CHESTPLATE(1),
	LEATHER_LEGGINGS(1),
	LEATHER_BOOTS(1),
	IRON_INGOT(1);
	
	private final int id;
	Tier1(int id) { this.id = id; }
	public int getValue() { return id; }
	
	public static Tier1 randomItem() {
		return Tier1.values()[Utils.randomInt(0, Tier1.values().length-1)];
	}
}