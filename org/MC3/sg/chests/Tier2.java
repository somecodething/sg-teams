package org.MC3.sg.chests;

import org.MC3.sg.Utils;

public enum Tier2 {
	GOLDEN_CARROT(2),
	COOKED_BEEF(8),
	DIAMOND(1),
	IRON_HELMET(1),
	IRON_CHESTPLATE(1),
	IRON_LEGGINGS(1),
	IRON_BOOTS(1),
	MELON(20),
	PUMPKIN_PIE(6),
	ENCHANTED_BOOK(1),
	GOLDEN_APPLE(1),
	ENDER_PEARL(1),
	EXP_BOTTLE(10),
	IRON_INGOT(1);
	
	private final int id;
	Tier2(int id) { this.id = id; }
	public int getValue() { return id; }
	
	public static Tier2 randomItem() {
		return Tier2.values()[Utils.randomInt(0, Tier2.values().length-1)];
	}
}
