package org.MC3.sg.chests;

import org.MC3.sg.Utils;

public enum Enchantments {
	ARROW_DAMAGE,
	ARROW_FIRE,
	ARROW_INFINITE,
	DAMAGE_ALL,
	FIRE_ASPECT,
	KNOCKBACK,
	PROTECTION_ENVIRONMENTAL,
	THORNS;
	
	public static Enchantments randomEnchant() {
		return Enchantments.values()[Utils.randomInt(0, Enchantments.values().length-1)];
	}
}