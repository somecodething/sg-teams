package org.MC3.sg.world;

import java.io.File;
import java.io.IOException;

import org.MC3.sg.Vars;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;

public class World {
	public static void setup() throws IOException {
		if(Bukkit.getWorld("Game") == null) {
			String mapName = Maps.randomValue().toString();
			
			Vars.map = Maps.valueOf(mapName);
			
			FileUtils.copyDirectory(new File("/root/minecraft/" + Bukkit.getServerName() + "/" + mapName), new File("/root/minecraft/" + Bukkit.getServerName() + "/Game"));
			Bukkit.createWorld(new WorldCreator("Game").type(WorldType.FLAT).generatorSettings("2;0;1").generateStructures(false));
			Bukkit.getWorld("Game").setGameRuleValue("doDaylightCycle", "false");
			Bukkit.getWorld("Game").setTime(6000);
		}
	}
}