package org.MC3.sg.world;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class MidLocs {
	public static HashMap<Maps, Location> locs = new HashMap<Maps, Location>();
	public static HashMap<Maps, Integer> maxHeight = new HashMap<Maps, Integer>();
	public static HashMap<Maps, Integer> radius = new HashMap<Maps, Integer>();
	public static void setup() {
		for(Maps m : Maps.values()) {
			if(m == Maps.Avail) {
				locs.put(m, new Location(Bukkit.getWorld("Game"), 0.5, 70, 0.5));
				maxHeight.put(m, 70);
				radius.put(m, 200);
			} else if(m == Maps.Arid) {
				locs.put(m, new Location(Bukkit.getWorld("Game"), 0.5, 79, 0.5));
				maxHeight.put(m, 75);
				radius.put(m, 200);
			}
		}
	}
}