package org.MC3.sg.world;

import org.MC3.sg.Utils;
import org.MC3.sg.Vars;

public enum Maps {
	Avail,
	Arid;
	
	public static Maps getCurrent() {
		return Vars.map;
	}
	
	public static Maps randomValue() {
		return Maps.values()[Utils.randomInt(0, Maps.values().length-1)];
	}
}