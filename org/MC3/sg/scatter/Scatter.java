package org.MC3.sg.scatter;

import java.util.ArrayList;

import org.MC3.sg.Utils;
import org.MC3.sg.Vars;
import org.MC3.sg.teams.Teams;
import org.MC3.sg.world.Maps;
import org.MC3.sg.world.MidLocs;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;

public class Scatter {
	public static ArrayList<Location> ChestLocations = new ArrayList<Location>();
	public static void setupChests() {
		for(int i = 0; i < 150; i++) {
			boolean gotLocation = false;
			while(gotLocation == false) {
				int x = Utils.randomInt(MidLocs.locs.get(Maps.getCurrent()).getBlockX()+66, MidLocs.locs.get(Maps.getCurrent()).getBlockX()+200);
				if(Math.random() < 0.5) {
					x = Integer.parseInt("-"+x);
				}
				int z = Utils.randomInt(MidLocs.locs.get(Maps.getCurrent()).getBlockX()+66, MidLocs.locs.get(Maps.getCurrent()).getBlockX()+200);
				if(Math.random() < 0.5) {
					z = Integer.parseInt("-"+z);
				}
				
				Location bottomLoc = new Location(Bukkit.getWorld("Game"), x, 0, z);
				Location tpLoc = Bukkit.getWorld("Game").getHighestBlockAt(bottomLoc).getLocation();
				tpLoc = tpLoc.add(0, 4, 0);
				
				if(Utils.locIsNearChests(tpLoc) == false) {
					if(tpLoc.getBlock().getType() == Material.AIR || tpLoc.getBlock().getType() == Material.LONG_GRASS && !ChestLocations.contains(tpLoc)) {
						if(tpLoc.getBlock().getBiome() != Biome.RIVER) {
							if(tpLoc.subtract(0, 1, 0).getBlock().getType() != Material.STATIONARY_WATER && tpLoc.subtract(0, 1, 0).getBlock().getType() != Material.STATIONARY_LAVA && tpLoc.subtract(0, 1, 0).getBlock().getType() != Material.WATER && tpLoc.subtract(0, 1, 0).getBlock().getType() != Material.LAVA) {
								ChestLocations.add(tpLoc);
								gotLocation = true;
							}
						}
					}
				}
			}
		}
	}
	
	public static void goChests() {
		for(Location loc : ChestLocations) {
			loc.getBlock().setType(Material.CHEST);
		}
	}
	
	public static ArrayList<Location> locations = new ArrayList<Location>();
	public static void setup() {
		for(int i = 0; i < 24; i++) {
			boolean gotLocation = false;
			while(gotLocation == false) {
				int x = Utils.randomInt(MidLocs.locs.get(Maps.getCurrent()).getBlockX()+70, MidLocs.locs.get(Maps.getCurrent()).getBlockX()+100);
				if(Math.random() < 0.5) {
					x = Integer.parseInt("-"+x);
				}
				int z = Utils.randomInt(MidLocs.locs.get(Maps.getCurrent()).getBlockX()+70, MidLocs.locs.get(Maps.getCurrent()).getBlockX()+100);
				if(Math.random() < 0.5) {
					z = Integer.parseInt("-"+z);
				}
				
				Location bottomLoc = new Location(Bukkit.getWorld("Game"), x, 0, z);
				Location tpLoc = Bukkit.getWorld("Game").getHighestBlockAt(bottomLoc).getLocation();
				tpLoc = tpLoc.add(0, 1, 0);
				
				if(tpLoc.getBlock().getType() == Material.AIR || tpLoc.getBlock().getType() == Material.LONG_GRASS) {
					if(tpLoc.subtract(0, 1, 0).getBlock().getType() != Material.LEAVES && tpLoc.subtract(0, 1, 0).getBlock().getType() != Material.LOG && tpLoc.subtract(0, 1, 0).getBlock().getType() != Material.STATIONARY_WATER && tpLoc.subtract(0, 1, 0).getBlock().getType() != Material.STATIONARY_LAVA && tpLoc.getY() < MidLocs.maxHeight.get(Maps.getCurrent())) {
						tpLoc = tpLoc.add(0, 4, 0);
						locations.add(tpLoc);
						gotLocation = true;
					}
				}
			}
		}
	}
	
	public static void go() {
		ArrayList<Player> dontScatter = new ArrayList<Player>();
		for(Player lp : Vars.participants) {
			if(!dontScatter.contains(lp)) {
				if(Teams.inTeam(lp) == false) {
					Location getLoc = locations.get(0);
					locations.remove(getLoc);
					lp.teleport(getLoc);
				} else {
					Player teamate = Teams.getPlayers(Teams.getTeam(lp)).get(0);
					
					dontScatter.add(teamate);
					
					Location getLoc = locations.get(0);
					locations.remove(getLoc);
					lp.teleport(getLoc);
					teamate.teleport(getLoc);
				}
			}
		}
	}
}