package org.MC3.sg;

public enum BreakableBlocks {
	LEAVES,
	LEAVES_2,
	LONG_GRASS,
	DOUBLE_PLANT,
	VINE;
}