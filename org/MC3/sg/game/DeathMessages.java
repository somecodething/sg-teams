package org.MC3.sg.game;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class DeathMessages implements Listener {
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		String deathMsg = e.getDeathMessage();
		String newMsg = "�c" + deathMsg.replace(" using ", " using �c�o");
		e.setDeathMessage(newMsg);
	}
}