package org.MC3.sg.game;

import java.util.ArrayList;

import org.MC3.sg.Main;
import org.MC3.sg.PlayerJoin;
import org.MC3.sg.Vars;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import me.Swedz.api.API;

public class Spec implements Listener {
	public static ArrayList<Player> specs = new ArrayList<Player>();
	
	public static void updateSpecs(Player player) {
		if(Vars.ingame == true) {
			if(!specs.contains(player)) {
				for(Player lp : specs) {
					player.hidePlayer(lp);
				}
			} else {
				for(Player lp : specs) {
					player.showPlayer(lp);
				}
			}
		} else {
			for(Player lp : Bukkit.getOnlinePlayers()) {
				player.showPlayer(lp);
			}
		}
	}
	
	public static void set(Player player, boolean value) {
		if(value == true) {
			if(Vars.participants.contains(player)) {
				Vars.participants.remove(player);
			}
			
			player.setGameMode(GameMode.CREATIVE);
			specs.add(player);
			
			player.sendMessage("�aYou are now in �bSpectator Mode");
			player.sendMessage("�aYou can use the compass to teleport to players.");
			player.sendMessage("�aUse �e/hub�a to return to the Hub.");
			
			Main.instance.getServer().getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {
				public void run() {
					if(player.isOnline()) {
						player.getInventory().setHeldItemSlot(0);
						player.getInventory().setItemInHand(API.getItemAPI().createItem(new ItemStack(Material.COMPASS), "�aTeleport to Players", new String[] {}, null, false, false));
						player.getInventory().setHeldItemSlot(8);
						player.getInventory().setItemInHand(API.getItemAPI().createItem(new ItemStack(Material.BED), "�aReturn to Hub", new String[] {}, null, false, false));
						player.getInventory().setHeldItemSlot(0);
					}
				}
			}, 5);
		} else {
			if(Vars.participants.contains(player)) {
				Vars.participants.remove(player);
			}
			
			player.setGameMode(GameMode.ADVENTURE);
			specs.remove(player);
			
			player.teleport(new Location(Bukkit.getWorld("world"), 0.5, 65, 0.5));
		}
		
		for(Player lp : Bukkit.getOnlinePlayers()) {
			updateSpecs(lp);
		}
	}
	
	public static void open(Player player) throws Exception {
		int size = (int) (Math.round(Math.floor(Vars.participants.size()+0.0)/9.0))*9;
		if(size == 0) {
			size = 9;
		} else if(size > 6*9) {
			size = 6*9;
		}
		
		Inventory menu = Bukkit.createInventory(player, size, "Teleport to Players");
		
		int slot = 0;
		for(Player lp : Vars.participants) {
			if(slot < size) {
				String playerName = lp.getName();
				
				ItemStack item = new ItemStack(Material.SKULL_ITEM);
				SkullMeta sm = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.SKULL_ITEM);
				sm.setOwner(playerName);
				item.setItemMeta(sm);
				item = API.getItemAPI().createItem(new ItemStack(item.getType(), 1, (byte)3), API.getChatAPI().namecolor(playerName) + playerName, new String[] {"�eClick to teleport to " + playerName}, null, false, false);
				menu.setItem(slot, item);
				
				slot++;
			} else {
				break;
			}
		}
		
		player.openInventory(menu);
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if(e.getDamager().getType() == EntityType.PLAYER) {
			Player player = (Player) e.getDamager();
			if(specs.contains(player)) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		Player player = (Player) e.getWhoClicked();
		if(specs.contains(player)) {
			e.setCancelled(true);
			
			if(e.getInventory().getName().equalsIgnoreCase("Teleport to Players")) {
				if(e.getCurrentItem() != null && e.getCurrentItem().getItemMeta() != null && e.getCurrentItem().getItemMeta().getDisplayName() != null) {
					if(e.getCurrentItem().getType() == Material.SKULL_ITEM) {
						String itemName = e.getCurrentItem().getItemMeta().getDisplayName();
						String playerName = API.getChatAPI().stripColor(itemName);
						Player pl = Bukkit.getPlayer(playerName);
						if(pl != null && pl.isOnline()) {
							player.teleport(pl);
							player.closeInventory();
						}
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) throws Exception {
		Action a = e.getAction();
		ItemStack is = e.getItem();
		
		if(specs.contains(e.getPlayer())) {
			e.setCancelled(true);
		}
		
		if(a == Action.PHYSICAL || is == null || is.getType() == Material.AIR) {
			return;
		}
		
		if(is.getType() == Material.BED && Vars.ingame == true && specs.contains(e.getPlayer())) {
			if(a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {
				PlayerJoin.GoToHub(e.getPlayer());
			}
		} else if(is.getType() == Material.COMPASS && Vars.ingame == true && specs.contains(e.getPlayer())) {
			open(e.getPlayer());
		}
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) throws Exception {
		if(specs.contains(e.getPlayer())) {
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPickup(PlayerPickupItemEvent e) throws Exception {
		if(specs.contains(e.getPlayer())) {
			e.setCancelled(true);
		}
	}
}