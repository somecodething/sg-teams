package org.MC3.sg.game;

import java.util.ArrayList;

import org.MC3.sg.Freeze;
import org.MC3.sg.Main;
import org.MC3.sg.Utils;
import org.MC3.sg.Vars;
import org.MC3.sg.scatter.Scatter;
import org.MC3.sg.teams.Teams;
import org.MC3.sg.world.Maps;
import org.MC3.sg.world.MidLocs;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.WorldBorder;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitTask;

import me.Swedz.api.API;

public class Game implements Listener {
	@EventHandler
	public void onInvOpen(InventoryOpenEvent e) {
		if(e.getInventory().getType() == InventoryType.ENCHANTING) {
			e.getInventory().setItem(1, new ItemStack(Material.INK_SACK, 3, (byte)4));
		}
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		if(Vars.ingame == false) {
			e.setCancelled(true);
		}
		
		if(e.getInventory().getType() == InventoryType.ENCHANTING) {
			if(e.getCurrentItem() != null && e.getCurrentItem().getType() == Material.INK_SACK) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void enchantItemEvent(EnchantItemEvent e) {
		if(e.getInventory().getType() == InventoryType.ENCHANTING) {
			e.getInventory().setItem(1, new ItemStack(Material.INK_SACK, 3, (byte)4));
		}
	}
	
	@EventHandler
	public void closeInventoryEvent(InventoryCloseEvent e) {
		if (e.getInventory().getType() == InventoryType.ENCHANTING) {
			e.getInventory().setItem(1, new ItemStack(Material.AIR));
		}
	}
	
	static BukkitTask bt;
	public static void checkStart() {
		bt = Bukkit.getScheduler().runTaskTimer(Main.instance, new Runnable() {
			@Override
	        public void run() {
	    		if(Vars.starting == false && Vars.ingame == false && Vars.canStart == true) {
	    			if(Bukkit.getOnlinePlayers().size() >= Vars.requiredPlayerCount) {
	    				start();
	    			}
	    		}
	    	}
		}, 20, 20);
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		if(Vars.ingame == true && Vars.participants.contains(player)) {
			player.getLocation().getWorld().strikeLightningEffect(player.getLocation());
			player.setHealth(0);
		} else {
			if(Vars.participants.contains(player)) {
				Vars.participants.remove(player);
			}
		}
	}
	
	@EventHandler
	public void onDamageByEntity(EntityDamageByEntityEvent e) {
		if(e.getEntityType() == EntityType.PLAYER && e.getDamager().getType() == EntityType.PLAYER) {
			Vars.lastDamager.put((Player)e.getEntity(), (Player)e.getDamager());
		}
	}
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event) {
		if(event.toWeatherState()) {
			event.setCancelled(true);
			World world = Bukkit.getWorlds().get(0);
			world.setStorm(false);
			world.setThundering(false);
		}
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) throws Exception {
		Player player = e.getEntity();
		if(Vars.ingame == true && Vars.participants.contains(player)) {
			player.getLocation().getWorld().strikeLightningEffect(player.getLocation());
			player.setHealth(20.0);
			
			if(Vars.lastDamager.containsKey(player)) {
				int kills = Vars.kills.get(Vars.lastDamager.get(player))+1;
				Vars.kills.put(Vars.lastDamager.get(player), kills);
			}
			
			Vars.participants.remove(player);
			Vars.deaths = Vars.deaths+1;
			
			for(Player lp : Bukkit.getOnlinePlayers()) {
				Scoreboard.set(lp);
			}
			
			Spec.set(player, true);
			
			stop();
		} else {
			player.setHealth(20.0);
		}
	}
	
	public static void wbWaitNone() {
		if(Vars.ingame == true) {
			Main.instance.getServer().getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {
				public void run() {
					Vars.nextEvent = "None";
					for(Player lp : Bukkit.getOnlinePlayers()) {
						Scoreboard.set(lp);
					}
				}
			}, 5*60*20);
		}
	}
		
	static BukkitTask wbBT;
	public static void wbShrink() {
		if(Vars.ingame == true) {
			Bukkit.broadcastMessage(Vars.prefix + "Border is going to start shrinking in �b5m");
			WorldBorder wb = Bukkit.getWorld("Game").getWorldBorder();
			wb.setCenter(MidLocs.locs.get(Maps.getCurrent()));
			wb.setSize(MidLocs.radius.get(Maps.getCurrent())*2);
			
			Vars.nextEvent = "Border Shrink";
			for(Player lp : Bukkit.getOnlinePlayers()) {
				Scoreboard.set(lp);
			}
			
			wbBT = Bukkit.getScheduler().runTaskTimer(Main.instance, new Runnable() {
				int countdown = 5*60;
				
				@Override
		        public void run() {
					countdown--;
					if(countdown == 240 || countdown == 180 || countdown == 120 || countdown == 60 || countdown == 30 || countdown == 15 || countdown <= 10 && countdown > 0) {
						int time = countdown;
						String span = "s";
						if(countdown >= 60) {
							time = (time/60) % 60;
							span = "m";
						}
						
						Bukkit.broadcastMessage(Vars.prefix + "The border is going to start shrinking in �b" + time + span);
					} else if(countdown > 0) {
						//do nothing
					} else if(countdown == 0) {
						Bukkit.broadcastMessage(Vars.prefix + "The border is going to start shrinking in �bnow");
						wb.setSize(20*2, 5*60);
						Vars.nextEvent = "Deathmatch";
						for(Player lp : Bukkit.getOnlinePlayers()) {
							Scoreboard.set(lp);
						}
						wbWaitNone();
					} else {
						wbBT.cancel();
					}
				}
			}, 20, 20);
		}
	}
	
	public static void stop() throws Exception {
		if(Vars.ingame == true) {
			boolean stop = false;
			if(Vars.participants.size() <= 1) {
				stop = true;
			} else {
				int counter = 0;
				for(int team : Teams.allTeams) {
					for(Player lp : Teams.getPlayers(team)) {
						if(Vars.participants.contains(lp)) {
							counter++;
							break;
						}
					}
				}
				if(counter <= 1) {
					stop = true;
				}
			}
			
			if(stop == true) {
				int winningTeam = -1;
				for(int team : Teams.allTeams) {
					for(Player lp : Teams.getPlayers(team)) {
						if(Vars.participants.contains(lp)) {
							winningTeam = team;
							break;
						}
					}
					if(winningTeam != -1) {
						break;
					}
				}
				
				Bukkit.broadcastMessage(Vars.prefix + "�3Team #" + winningTeam + " �ahas won the game!");
				for(Player lp : Bukkit.getOnlinePlayers()) {
					lp.playSound(lp.getLocation(), Sound.WITHER_DEATH, 1, 1);
				}
				
				try {
					Utils.setLine("motd", "Ending");
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				org.MC3.sg.Title title = new org.MC3.sg.Title("�3Team #" + winningTeam,"�awon the game");
				title.setFadeInTime(0); title.setStayTime(4*20); title.setFadeOutTime(10);
				title.setTimingsToTicks();
				title.broadcast();
				
				Bukkit.broadcastMessage(Vars.prefix + "You will be returned to Hub in �b30s");
				
				Main.instance.getServer().getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {
					public void run() {
						Bukkit.broadcastMessage(Vars.prefix + "Returning you to Hub now...");
						for(Player lp : Bukkit.getOnlinePlayers()) {
							API.getBungeeAPI().connect(lp, "Hub");
						}
					}
				}, 30*20);
				
				Main.instance.getServer().getScheduler().scheduleSyncDelayedTask(Main.instance, new Runnable() {
					public void run() {
						Bukkit.shutdown();
					}
				}, 31*20);
			}
		}
	}
	
	static BukkitTask refillBT;
	public static void refillCountdown() {
		Bukkit.broadcastMessage(Vars.prefix + "Chests will be refilled in �b10m");
		refillBT = Bukkit.getScheduler().runTaskTimer(Main.instance, new Runnable() {
			int countdown = 10*60;
			
			@Override
			public void run() {
				if(Vars.participants.size() <= 1) {
					refillBT.cancel();
					return;
				}
				
				countdown--;
				if(countdown == 300 || countdown == 240 || countdown == 180 || countdown == 120 || countdown == 60 || countdown == 30 || countdown == 15 || countdown <= 10 && countdown > 0) {
					int time = countdown;
					String span = "s";
					if(countdown >= 60) {
						time = (time/60) % 60;
						span = "m";
					}
					
					Bukkit.broadcastMessage(Vars.prefix + "Chests will be refilled in �b" + time + span);
				} else if(countdown > 0) {
					//do nothing
				} else if(countdown == 0) {
					Bukkit.broadcastMessage(Vars.prefix + "Chests have been refilled!");
					Vars.openedChests = new ArrayList<Location>();
					Vars.tier2Chance = 0.5;
					wbShrink();
					refillBT.cancel();
				}
			}
		}, 20, 20);
	}
	
	public static void setupChests() {
		Scatter.setupChests();
	}
	
	static BukkitTask startBT2;
	public static void countDown() {
		setupChests();
		Bukkit.broadcastMessage(Vars.prefix + "You will be unfrozen in �b10s�a...");
		startBT2 = Bukkit.getScheduler().runTaskTimer(Main.instance, new Runnable() {
			int countdown = 25;
			
			@Override
			public void run() {
				countdown--;
				
				if(countdown > 15) {
					Bukkit.broadcastMessage(Vars.prefix + "Starting in �b" + (countdown-15) + "s�a...");
				} else if(countdown == 15) {
					Vars.starting = false;
					Vars.ingame = true;
					Bukkit.broadcastMessage(Vars.prefix + "The game has been started!");
					for(Player lp : Bukkit.getOnlinePlayers()) {
						Freeze.set(lp, false);
						Scoreboard.set(lp);
						lp.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 300, 1));
					}
					Vars.startTime = System.currentTimeMillis();
					Scatter.goChests();
					try {
						stop();
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else if(countdown > 0 && countdown < 15) {
					//do nothing
				} else if(countdown == 0) {
					for(Player player : Bukkit.getOnlinePlayers()) {
						player.setHealth(20.0);
						player.setFoodLevel(20);
					}
					Bukkit.broadcastMessage(Vars.prefix + "You can now take damage!");
					Vars.GodMode = new ArrayList<Player>();
					
					refillCountdown();
				} else {
					startBT2.cancel();
				}
			}
		}, 20, 20);
	}
	
	public static void setup(Player player) {
		player.getInventory().setContents(new ItemStack[] {});
		player.getInventory().setArmorContents(new ItemStack[] {});
	}
	
	static BukkitTask startBT;
	public static void start() {
		if(Vars.canStart == true) {
			Vars.starting = true;
			Vars.canStart = false;
			
			try {
				Utils.setLine("motd", "Starting");
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			for(Player lp : Bukkit.getOnlinePlayers()) {
				Scoreboard.set(lp);
			}
			
			Bukkit.broadcastMessage(Vars.prefix + "�aThe game is starting in �b60s");
			startBT = Bukkit.getScheduler().runTaskTimer(Main.instance, new Runnable() {
				int countdown = 60;
				
				@Override
				public void run() {
					countdown--;
					if(Bukkit.getOnlinePlayers().size() >= Vars.requiredPlayerCount) {
						if(countdown == 30 || countdown == 15 || countdown <= 10) {
							if(countdown != 0) {
								Bukkit.broadcastMessage(Vars.prefix + "�aThe game is starting in �b" + countdown + "s");
							} else {
								Bukkit.broadcastMessage(Vars.prefix + "�aThe game is starting �bnow");
			    				
								for(Entity le : Bukkit.getWorld("Game").getEntities()) {
									le.teleport(new Location(Bukkit.getWorld("Game"), 0.5, -60, 0.5));
								}
	
								Vars.participants = new ArrayList<Player>();
								for(Player lp : Bukkit.getOnlinePlayers()) {
									Vars.lastDamager.remove(lp);
									Vars.kills.put(lp, 0);
									Vars.participants.add(lp);
									setup(lp);
			    					
									lp.teleport(new Location(Bukkit.getWorld("Game"), 0.5, 100, 0.5));
									Freeze.set(lp, true);
								}
			    				
								Scatter.go();
			    				
								for(Player lp : Bukkit.getOnlinePlayers()) {
									Scoreboard.set(lp);
									lp.setGameMode(GameMode.SURVIVAL);
									Vars.GodMode.add(lp);
								}
			    				
								try {
									Utils.setLine("motd", "Ingame");
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								countDown();
								
								startBT.cancel();
							}
						}
					} else {
						Bukkit.broadcastMessage(Vars.prefix + "�cThe game has been cancelled due to lack of players.");
		    			
						try {
							Utils.setLine("motd", "Waiting");
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						Vars.starting = false;
						Vars.canStart = true;
		    			
						startBT.cancel();
					}
				}
			}, 20, 20);
		}
	}
}