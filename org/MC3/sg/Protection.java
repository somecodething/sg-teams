package org.MC3.sg;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

public class Protection implements Listener {
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		if(Vars.ingame == false) {
			e.setCancelled(true);
			return;
		} else {
			for(BreakableBlocks bb : BreakableBlocks.values()) {
				if(e.getBlock().getType() == Material.valueOf(bb.toString())) {
					return;
				}
			}
			
			e.setCancelled(true);
			return;
		}
	}
	
	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		e.setCancelled(true);
		return;
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if(e.getEntityType() != EntityType.PLAYER) {
			return;
		}
		
		if(Vars.ingame == false) {
			e.setCancelled(true);
		} else {
			if(Vars.GodMode.contains((Player)e.getEntity())) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onEntitySpawn(EntitySpawnEvent e) {
		if(e.getEntityType() == EntityType.ARROW || e.getEntityType() == EntityType.DROPPED_ITEM || e.getEntityType() == EntityType.BOAT || e.getEntityType() == EntityType.EGG || e.getEntityType() == EntityType.SNOWBALL || e.getEntityType() == EntityType.ENDER_PEARL) {
			return;
		}
		
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		if(Vars.ingame == false) {
			e.setCancelled(true);
		}
	}
}