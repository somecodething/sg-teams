package org.MC3.sg;

import java.io.IOException;

import org.MC3.sg.chests.ChestListener;
import org.MC3.sg.game.DeathMessages;
import org.MC3.sg.game.Game;
import org.MC3.sg.game.Spec;
import org.MC3.sg.scatter.Scatter;
import org.MC3.sg.teams.TeamCommands;
import org.MC3.sg.teams.Teams;
import org.MC3.sg.world.MidLocs;
import org.MC3.sg.world.World;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	public static Main instance;
	public void onEnable() {
		instance = this;
		
		try {
			World.setup();
			MidLocs.setup();
			Scatter.setup();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Game.checkStart();
		
		getCommand("tp").setExecutor(new Commands());
		registerEvents(instance,
			new Protection(),
			new ChestListener(),
			new Game(),
			new DeathMessages(),
			new PlayerJoin(),
			new Freeze(),
			new Spec(),
			
			new Teams(),
			new TeamCommands()
		);
		
		getCommand("team").setExecutor(new TeamCommands());
		
		try {
			Utils.setLine("motd", "Waiting");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onDisable() {
		Bukkit.getScheduler().cancelAllTasks();
		try {
			Utils.setLine("motd", "Rebooting");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void registerEvents(org.bukkit.plugin.Plugin plugin, Listener... listeners) {
		for(Listener listener : listeners) {
			Bukkit.getServer().getPluginManager().registerEvents(listener, plugin);
		}
	}
}