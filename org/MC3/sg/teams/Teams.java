package org.MC3.sg.teams;

import java.util.ArrayList;
import java.util.HashMap;

import org.MC3.sg.Vars;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Teams implements Listener {
	public static ArrayList<Integer> allTeams = new ArrayList<Integer>();
	public static HashMap<Integer, ArrayList<Player>> teamPlayers = new HashMap<Integer, ArrayList<Player>>();
	public static HashMap<Player, ArrayList<Player>> invites = new HashMap<Player, ArrayList<Player>>();
	public static int teamSize = 2;
	public static boolean leaveDisband = true;
	
	public static boolean inTeam(Player player) {
		for(int t : allTeams) {
			if(teamPlayers.get(t) != null && teamPlayers.get(t).contains(player)) {
				return true;
			}
		} return false;
	}
	
	public static int getTeam(Player player) {
		for(int t : allTeams) {
			if(teamPlayers.get(t).contains(player)) {
				return t;
			}
		} return 0;
	}
	
	public static ArrayList<Player> getPlayers(int team) {
		return teamPlayers.get(team);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		invites.put(e.getPlayer(), new ArrayList<Player>());
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		invites.put(e.getPlayer(), new ArrayList<Player>());
		if(leaveDisband == true) {
			if(inTeam(e.getPlayer()) == true) {
				int team = getTeam(e.getPlayer());
				for(Player lp : getPlayers(team)) {
					lp.sendMessage(Vars.prefix + "�c" + e.getPlayer().getName() + " has left the team.");
				}
				allTeams.remove(Integer.valueOf(team));
				teamPlayers.remove(Integer.valueOf(team));
			}
		}
	}
}
