package org.MC3.sg.teams;

import java.util.ArrayList;
import java.util.HashMap;

import org.MC3.sg.Vars;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class TeamCommands implements CommandExecutor, Listener {
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if(e.getEntityType() == EntityType.PLAYER && e.getDamager().getType() == EntityType.PLAYER) {
			Player victim = (Player) e.getEntity();
			Player attacker = (Player) e.getDamager();
			if(Teams.getTeam(victim) == Teams.getTeam(attacker)) {
				e.setCancelled(true);
			}
		}
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)) {
			sender.sendMessage("�cYou must be a player to do this command!");
			return true;
		}
		
		if(label.equalsIgnoreCase("team")) {
			Player player = (Player) sender;
			if(Vars.ingame == true || player.getWorld().getName() == "Game" && args[0] != "list") {
				player.sendMessage(Vars.prefix + "�cYou cannot do this command while ingame.");
				return true;
			}
			
			if(args.length == 0) {
				player.sendMessage("�4�l======== �c�lTeam Commands �4�l========");
				player.sendMessage("�e/team create - Creates a team.");
				player.sendMessage("�e/team invite [player] - Invites a player to your team.");
				player.sendMessage("�e/team accept [player] - Accepts an invite to a team.");
				player.sendMessage("�e/team deny [player] - Denies an invite from a team.");
				player.sendMessage("�e/team leave - Leaves your team.");
				player.sendMessage("�e/team chat [msg] - Send a message to your team.");
				player.sendMessage("�e/team list - Lists all alive teams.");
			} else if(args[0].equalsIgnoreCase("create")) {
				if(Teams.inTeam(player) == false) {
					int teamID = 0;
					for(int id : Teams.allTeams) {
						if(id >= teamID) {
							teamID = id;
						}
					} teamID++;

					Teams.allTeams.add(teamID);
					ArrayList<Player> inTeam = new ArrayList<Player>();
					inTeam.add(player);
					Teams.teamPlayers.put(teamID, inTeam);
					
					player.sendMessage(Vars.prefix + "�aYour team has been created.");
				} else {
					player.sendMessage(Vars.prefix + "�cYou are already in a team.");
				}
			} else if(args[0].equalsIgnoreCase("invite")) {
				if(args.length == 2) {
					if(Teams.inTeam(player) == false) {
						Bukkit.getServer().dispatchCommand(sender, "team create");
					}
					
					Player invited = Bukkit.getPlayer(args[1]);
					if(invited != null && invited.isOnline()) {
						if(Teams.invites.get(invited) == null || !Teams.invites.get(invited).contains(player)) {
							if(Teams.inTeam(invited) == false) {
								ArrayList<Player> invites = Teams.invites.get(invited);
								if(invites == null) {
									invites = new ArrayList<Player>();
								}
								invites.add(player);
								Teams.invites.put(invited, invites);
								
								player.sendMessage(Vars.prefix + "�aYou have invited " + invited.getName() + " to your team.");
								invited.sendMessage(Vars.prefix + "�aYou have been invited to " + player.getName() + "'s Team!");
								invited.sendMessage(Vars.prefix + "�aType �e/team accept " + player.getName() + " �ato accept.");
							} else {
								player.sendMessage(Vars.prefix + "�cThat player is already in a team.");
							}
						} else {
							player.sendMessage(Vars.prefix + "�cYou've already invited that player.");
						}
					} else {
						player.sendMessage(Vars.prefix + "�cThat player is not online.");
					}
				} else {
					player.sendMessage(Vars.prefix + "�cYou must specify a player: /team invite [player]");
				}
			} else if(args[0].equalsIgnoreCase("accept")) {
				if(args.length == 2) {
					if(Teams.inTeam(player) == false) {
						Player accepted = Bukkit.getPlayer(args[1]);
						if(accepted != null && accepted.isOnline()) {
							if(Teams.invites.get(player) != null && Teams.invites.get(player).contains(accepted)) {
								if(Teams.teamSize > Teams.getPlayers(Teams.getTeam(accepted)).size()) {
									ArrayList<Player> invites = Teams.invites.get(player);
									if(invites == null) {
										invites = new ArrayList<Player>();
									}
									invites.remove(accepted);
									Teams.invites.put(player, invites);
									
									ArrayList<Player> inTeam = Teams.getPlayers(Teams.getTeam(accepted));
									if(inTeam == null) {
										player.sendMessage(Vars.prefix + "�cThat team no longer exists.");
										return true;
									}
									
									inTeam.add(player);
									Teams.teamPlayers.put(Teams.getTeam(accepted), inTeam);
									
									player.sendMessage(Vars.prefix + "�aYou have joined " + accepted.getName() + "'s Team!");
									accepted.sendMessage(Vars.prefix + "�a" + player.getName() + " has joined your Team!");
								} else {
									player.sendMessage(Vars.prefix + "�cThat team is already full.");
								}
							} else {
								player.sendMessage(Vars.prefix + "�cYou have not been invited by that player.");
							}
						} else {
							player.sendMessage(Vars.prefix + "�cThat player is not online.");
						}
					} else {
						player.sendMessage(Vars.prefix + "�cYou are already in a team.");
					}
				} else {
					player.sendMessage(Vars.prefix + "�cYou must specify a player: /team accept [player]");
				}
			} else if(args[0].equalsIgnoreCase("deny")) {
				if(args.length == 2) {
					Player accepted = Bukkit.getPlayer(args[1]);
					if(accepted != null && accepted.isOnline()) {
						if(Teams.invites.get(player).contains(accepted)) {
							ArrayList<Player> invites = Teams.invites.get(player);
							invites.remove(accepted);
							Teams.invites.put(player, invites);
									
							player.sendMessage(Vars.prefix + "�cYou have denied " + accepted.getName() + "'s Team invite.");
							accepted.sendMessage(Vars.prefix + "�c" + player.getName() + " has denied your Team invite.");
						} else {
							player.sendMessage(Vars.prefix + "�cYou have not been invited by that player.");
						}
					} else {
						player.sendMessage(Vars.prefix + "�cThat player is not online.");
					}
				} else {
					player.sendMessage(Vars.prefix + "�cYou must specify a player: /team deny [player]");
				}
			} else if(args[0].equalsIgnoreCase("leave")) {
				if(Teams.inTeam(player) == true) {
					int teamID = Teams.getTeam(player);
					
					Player other;
					if(Teams.getPlayers(teamID).size() == 2) {
						other = Teams.getPlayers(teamID).get(0);
						if(player == other) {
							other = Teams.getPlayers(teamID).get(1);
						}
						
						other.sendMessage(Vars.prefix + "�c" + player.getName() + " has left the team.");
						for(Player lp : Teams.invites.keySet()) {
							if(Teams.invites.get(lp).contains(other)) {
								ArrayList<Player> invites = Teams.invites.get(lp);
								invites.remove(other);
								Teams.invites.put(lp, invites);
							}
						}
					}
					
					player.sendMessage(Vars.prefix + "�c" + player.getName() + " has left the team.");
					
					for(Player lp : Teams.invites.keySet()) {
						if(Teams.invites.get(lp).contains(player)) {
							ArrayList<Player> invites = Teams.invites.get(lp);
							invites.remove(player);
							Teams.invites.put(lp, invites);
						}
					}
					
					Teams.teamPlayers.remove(Integer.valueOf(teamID));
					Teams.allTeams.remove(Integer.valueOf(teamID));
				} else {
					player.sendMessage(Vars.prefix + "�cYou are not in a team.");
				}
			} else if(args[0].equalsIgnoreCase("list")) {
				if(Teams.allTeams.size() <= 0) {
					player.sendMessage(Vars.prefix + "�cThere are no teams to display.");
					return true;
				}
				
				HashMap<Integer, String> playersInTeams = new HashMap<Integer, String>();
				player.sendMessage("�4�l======== �c�lAll Teams �4�l========");
				for(int team : Teams.allTeams) {
					String players = "";
					for(Player lp : Teams.getPlayers(team)) {
						if(Vars.ingame == true) {
							if(Vars.participants.contains(lp)) {
								if(players == "") {
									players = "�a" + lp.getName() + "�b";
								} else {
									players = players + ", �a" + lp.getName() + "�b";
								}
							} else {
								if(players == "") {
									players = "�c" + lp.getName() + "�b";
								} else {
									players = players + ", " + "�c" + lp.getName() + "�b";
								}
							}
						} else {
							if(players == "") {
								players = "�f" + lp.getName() + "�b";
							} else {
								players = players + ", �f" + lp.getName() + "�b";
							}
						}
					} playersInTeams.put(team, players);
					player.sendMessage("�3Team #" + team + ": �b[" + playersInTeams.get(team) + "]");
				}
			} else if(args[0].equalsIgnoreCase("chat")) {
				if(args.length >= 2) {
					if(Teams.inTeam(player) == true) {
						String message = "";
						for(int l = 1; l < args.length; l++) {
							if(args[l] != null) {
								if(l != 1) {
									message = message + " " + args[l];
								} else {
									message = args[l];
								}
							}
						}
						
						for(Player lp : Teams.getPlayers(Teams.getTeam(player))) {
							lp.sendMessage("�3[Team]: �a" + player.getName() + ": �f" + message);
						}
					} else {
						player.sendMessage(Vars.prefix + "�cYou're not in a team.");
					}
				} else {
					player.sendMessage(Vars.prefix + "�cPlease specify a message.");
				}
			} else {
				player.sendMessage(Vars.prefix + "�cInvalid teams command.");
			}
		}
		
		return true;
	}
}
